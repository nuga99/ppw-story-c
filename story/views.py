from django.shortcuts import render
from datetime import datetime, date

# Enter your name here

mhs_name = 'Muhammad Ardivan Satrio Nugroho' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 8 ,14) #TODO Implement this, format (Year, Month, Date)
npm = 1706025371 # TODO Implement this
education = "Universitas Indonesia"
desc = "Pria punya selera"
hobby = "Bersantai, Bermain Gitar"

mhs_name_1 = 'Ringgi Cahyo Dwiputra' # TODO Implement this
curr_year_1 = int(datetime.now().strftime("%Y"))
birth_date_1 = date(1999, 2 ,25) #TODO Implement this, format (Year, Month, Date)
npm_1 = 1706025005# TODO Implement this
education_1 = "Universitas Indonesia"
desc_1 = "Motor Selalu"
hobby_1 = "Motor dan ngoding"

mhs_name_2 = 'Farhan Azyumardhi Azmi' # TODO Implement this
curr_year_2 = int(datetime.now().strftime("%Y"))
birth_date_2 = date(1999, 8 ,21) #TODO Implement this, format (Year, Month, Date)
npm_2 = 1706979234# TODO Implement this
education_2 = "Universitas Indonesia"
desc_2 = "Gamer Sejati"
hobby_2 = "Ngoding dan bermain game"

# Create your views here.


def nuga(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
               'education':education , 'desc':desc, 'hobby':hobby}
    return render(request, 'nuga.html', response)

def ringgi(request):
    response = {'name_1': mhs_name_1, 'age_1': calculate_age(birth_date_1.year), 'npm_1': npm_1,
               'education_1':education_1 , 'desc_1':desc_1, 'hobby_1':hobby_1}
    return render(request, 'ringgi.html', response)

def farhan(request):
    response = {'name_2': mhs_name_2, 'age_2': calculate_age(birth_date_2.year), 'npm_2': npm_2,
               'education_2':education_2 , 'desc_2':desc_2, 'hobby_2':hobby_2}
    return render(request, 'farhan.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
