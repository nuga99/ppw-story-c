from django.contrib import admin
from .models import SchedulePost


class SchedulePostAdmin(admin.ModelAdmin):
     list_display= ['nama_kegiatan', 'tanggal', 'tempat', 'kategori']

admin.site.register(SchedulePost,SchedulePostAdmin)

# Register your models here.
