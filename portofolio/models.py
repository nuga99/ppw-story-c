from django.db import models
from django.utils import timezone
from datetime import date, datetime
from django import forms
from django.forms import ModelForm
from django.core.validators import MaxLengthValidator


# Create your models here.
class SchedulePost(models.Model):
    nama_kegiatan = models.CharField(max_length = 200, validators=[MaxLengthValidator(150,message='Length must be no more than 255 characters.')])
    tanggal = models.CharField(max_length = 20)
    tempat = models.CharField(max_length = 200, validators=[MaxLengthValidator(150, message='Length must be no more than 255 characters.')])
    kategori = models.CharField(max_length = 200, validators=[MaxLengthValidator(150, message='Length must be no more than 255 characters.')])




