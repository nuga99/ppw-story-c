from django.shortcuts import render,redirect
from django.core import serializers
from django.http import HttpResponse
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from .models import SchedulePost



# Create your views here.
def about(request):
    return render(request,'about.html')

def blog(request):
    return render(request,'blog.html')

def contacts(request):
    return render(request,'contacts.html')

def education(request):
    return render(request,'education.html')

def experience(request):
    return render(request,'experience.html')

def projects(request):
    return render(request,'projects.html')

def guest(request):
    return render(request,'guest_book.html')

# Serializers using my schedule list
def json_schedule(request):
    schedule_list = SchedulePost.objects.all()
    json = serializers.serialize('json', schedule_list)
    return HttpResponse(json, content_type='application/json')

# render the show_schedule from list
def show_schedule(request):
    schedule_list = SchedulePost.objects.all()
    paginator = Paginator(schedule_list, 5)

    page = request.GET.get('page')
    schedule_paginate = paginator.get_page(page)
    return render(request,'schedule/show_schedule.html',{"schedule":schedule_paginate})

# delete all schedule
def delete_schedule(request):
    SchedulePost.objects.all().delete()
    return redirect('get_schedule')