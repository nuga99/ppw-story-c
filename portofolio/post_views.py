from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render,redirect
from .forms import FormSchedule
from django.contrib import messages


def schedule_data(request):

    form = FormSchedule(request.POST)
    # form get POST request from views
    if form.is_valid():

        messages.success(request, "The form is valid.")
        form.save()
        return redirect('get_schedule')

    else:
        messages.error(request, "The form is invalid or error occured.")
        form = FormSchedule()

    return render(request, "schedule/schedule.html", {"the_form": form})
