from django import forms
from django.db import models
from .models import SchedulePost
from django.forms import ModelForm,fields


class FormSchedule(forms.ModelForm):

    class Meta:
        model = SchedulePost
        fields = ['nama_kegiatan', 'tanggal', 'tempat', 'kategori']
        widgets = {

            'nama_kegiatan': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama Kegiatan', 'maxlength':150}),

            'tanggal': forms.DateTimeInput(attrs={'class': 'form-control', 'id': 'datepicker', 'placeholder': 'Tanggal'}),

            'tempat': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Tempat Kegiatan', 'maxlength':150}),

            'kategori': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Kategori Kegiatan', 'maxlength':150}),

        }

    # def clean_content(value):
        
    #     if(len(value)>255):
    #         raise forms.ValidationError('Maximum characters length 255.')
        
    #     elif(len(self.cleaned_data['tempat'])>255):
    #         raise forms.ValidationError('Maximum characters length 255.')

    #     elif(len(self.cleaned_data['kategori'])>255):
    #         raise forms.ValidationError('Maximum characters length 255.')

    #     return value