"""story_nuga URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from portofolio import get_views
from portofolio import post_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', get_views.about, name="about"),
    path('blog/', get_views.blog, name="blog"),
    path('contacts/', get_views.contacts, name="contacts"),
    path('education/',get_views.education,name="education"),
    path('experience/',get_views.experience,name="experience"),
    path('projects/',get_views.projects,name="projects"),
    path('guest_book/',get_views.guest,name="guest_book"),
    path('json_schedule/',get_views.json_schedule,name="datatables_json"),
    path('schedule/', post_views.schedule_data, name="schedule_post"),
    path('schedule/show_schedule_now/', get_views.show_schedule, name="get_schedule"),
    path('schedule/delete',get_views.delete_schedule,name="delete_schedule") #url untuk delete
]
